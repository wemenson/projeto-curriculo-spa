package br.com.curriculo.Cursos.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import br.com.curriculo.Cursos.model.Curso;
import br.com.curriculo.Cursos.repository.CursoRepository;

@RestController
@RequestMapping("/api/v1")
public class CursoController {
	
	@Autowired
    private CursoRepository cursoRepository;
	
	 @GetMapping("/cursos")
	    public List<Curso> getAllEmployees() {
	        return (List<Curso>)  cursoRepository.findAll();
	    }
	 
	 @GetMapping("/cursos/{id}")
	    public ResponseEntity<Curso> getEmployeeById(@PathVariable(value = "id") int cursoId)
	        throws ResourceNotFoundException {
		 Curso curso = cursoRepository.findById(cursoId)
	          .orElseThrow(() -> new ResourceNotFoundException("Curso nao localizado por este identificador: " + cursoId));
	        return ResponseEntity.ok().body(curso);
	    }
	
	 @PostMapping("/cursos")
	    public Curso createEmployee(@Valid @RequestBody Curso curso) {
	        return cursoRepository.save(curso);
	    }
	 
	 @PutMapping("/cursos/{id}")
	    public ResponseEntity<Curso> updateEmployee(@PathVariable(value = "id") int cursoId,
	         @Valid @RequestBody Curso cursoDetalhe) throws ResourceNotFoundException {
		 	Curso curso = cursoRepository.findById(cursoId)
	        .orElseThrow(() -> new ResourceNotFoundException("Curso nao localizado por este identificador: " + cursoId));

		 	curso.setTitulo(cursoDetalhe.getTitulo());
		 	curso.setInstituicao(cursoDetalhe.getInstituicao());
		 	curso.setStatus(cursoDetalhe.getStatus());
		 	curso.setTipo(cursoDetalhe.getTipo());
		 	curso.setDescricao(cursoDetalhe.getDescricao());
	        final Curso novoCurso = cursoRepository.save(curso);
	        return ResponseEntity.ok(novoCurso);
	    }
	 
	 @DeleteMapping("/cursos/{id}")
	    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") int cursoId)
	         throws ResourceNotFoundException {
		 	Curso curso = cursoRepository.findById(cursoId)
	       .orElseThrow(() -> new ResourceNotFoundException("Curso nao localizado por este identificador: " + cursoId));

		 	cursoRepository.delete(curso);
	        Map<String, Boolean> response = new HashMap<>();
	        response.put("deleted", Boolean.TRUE);
	        return response;
	    }
}
