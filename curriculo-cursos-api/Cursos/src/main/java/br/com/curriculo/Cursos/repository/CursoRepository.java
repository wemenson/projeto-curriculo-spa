package br.com.curriculo.Cursos.repository;

import java.util.Optional;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import br.com.curriculo.Cursos.model.Curso;

@EnableScan
public interface CursoRepository extends CrudRepository<Curso, String> {
    
    Optional<Curso> findById(Integer id);
}